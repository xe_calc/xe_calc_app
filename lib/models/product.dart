import 'package:xecalc/utils.dart';

enum ProductPortionSourceType {
  custom,
  fromCatalog
}

enum CustomProductMeasurementUnit {
  grams,
  milliliters
}

abstract class ProductSource {
  String productName;
  bool isMeasured;
  ProductPortionSourceType type;
  int count;

  double carbohydrates;
  double fat;
  double protein;
  double calories;

  ProductSource({
    required this.productName,
    required this.isMeasured,
    required this.type,
    required this.count,
    required this.carbohydrates,
    required this.fat,
    required this.protein,
    required this.calories,
  });

  Map<String, dynamic> toJson();
}

class CustomProductSource extends ProductSource {
  CustomProductMeasurementUnit measurementUnit;

  CustomProductSource(
      {
        required productName,
        required int count,
        required carbohydrates,
        required fat,
        required protein,
        required calories,
        required this.measurementUnit
      }
      ) : super(
        productName: productName,
        count: count,
        isMeasured: true,
        type: ProductPortionSourceType.custom,
        carbohydrates: carbohydrates,
        fat: fat,
        protein: protein,
        calories: calories,
      );

  CustomProductSource.fromJson(Map<String, dynamic> json) :
        measurementUnit = CustomProductMeasurementUnit.values.firstWhere(
                (e) => e.toString() == json['measurementUnit']
        ),
        super(
          productName: json['productName'],
          isMeasured: true,
          type: ProductPortionSourceType.custom,
          count: json['count'],
          carbohydrates: json['carbohydrates'],
          fat: json['fat'],
          protein: json['protein'],
          calories: json['calories']
        );

  Map<String, dynamic> toJson() {
    return {
      'measurementUnit': measurementUnit.toString(),
      'productName': productName,
      'carbohydrates': carbohydrates,
      'fat': fat,
      'calories': calories,
      'protein': protein,
      'count': count,
      'isMeasured': isMeasured,
    };
  }
}


class CatalogProductSource extends ProductSource {
  final String productId;
  int portionId;
  String portionName;

  CatalogProductSource(
      {
        required this.productId,
        required productName,
        required this.portionName,
        required this.portionId,
        required int count,

        required carbohydrates,
        required fat,
        required protein,
        required calories,
        required isMeasured,

      }
      ) : super(
        productName: productName,
        count: count,
        isMeasured: isMeasured,
        type: ProductPortionSourceType.fromCatalog,
        carbohydrates: carbohydrates,
        fat: fat,
        protein: protein,
        calories: calories,
      );

  CatalogProductSource.fromJson(Map<String, dynamic> json)
      : productId = json['productId'],
        portionId = json['portionId'],
        portionName = json['portionName'],
        super(
          productName: json['productName'],
          isMeasured: json['isMeasured'],
          type: ProductPortionSourceType.fromCatalog,
          count: json['count'],
          carbohydrates: json['carbohydrates'],
          fat: json['fat'],
          protein: json['protein'],
          calories: json['calories'],
        );

  Map<String, dynamic> toJson() {
    return {
      'productId': productId,
      'productName': productName,
      'portionName': portionName,
      'portionId': portionId,
      'count': count,
      'isMeasured': isMeasured,
      'carbohydrates': carbohydrates,
      'fat': fat,
      'calories': calories,
      'protein': protein,
    };
  }
}

class DishProduct {
  final String key;
  ProductSource source;

  DishProduct(
      this.key,
      this.source
      );

  String get productName => source.productName;

  String get portionName {
    String label;
    if (isCustomProduct) {
      label = getMeasurementLabel(
          (source as CustomProductSource).measurementUnit
      );
    } else {
      if (source.isMeasured) {

        List<String> words = (source as CatalogProductSource)
            .portionName.split(' ');
        label = words.last;
      } else {
        label = (source as CatalogProductSource).portionName;
      }
    }
    return label;
  }

  String get portionText{
    if (source.isMeasured) {
      return source.count.toString() + ' ' + portionName;
    } else {
      return portionName + ' x ' + source.count.toString();
    }
  }

  bool get isCustomProduct => source.type == ProductPortionSourceType.custom;

  double get carbohydrates => calcParam(
      source.carbohydrates,
      source.count,
      isMeasured: source.isMeasured
  );

  double get fat => calcParam(
      source.fat,
      source.count,
      isMeasured: source.isMeasured
  );

  double get calories => calcParam(
      source.calories,
      source.count,
      isMeasured: source.isMeasured
  );

  double get protein => calcParam(
      source.protein,
      source.count,
      isMeasured: source.isMeasured
  );

  double get xeCount => carbohydratesToXe(carbohydrates);

  DishProduct.fromJson(Map<String, dynamic> json, String key)
      : key = key,
        source =
          json['source']['productId'] == null ?
          CustomProductSource.fromJson(json['source']) :
          CatalogProductSource.fromJson(json['source']);


  Map<String, dynamic> toJson() {
    return {'source': source.toJson()};
  }
}


class ProductSearchItem {
  final String key;
  final String name;

  ProductSearchItem(this.key, this.name);
}

class ProductPortion {
  final int id;
  final String label;
  final bool isMeasured;

  final double carbohydrates;
  final double fat;
  final double protein;
  final double calories;
  final double xeCount;

  ProductPortion(
      this.id,
      this.label,
      this.isMeasured,
      this.carbohydrates,
      this.fat,
      this.protein,
      this.calories,
      this.xeCount,
      );
}

class ProductForSelect {
  final String productId;
  final String name;
  final List<ProductPortion> portions;

  ProductForSelect(this.productId, this.name, this.portions);

}

class TotalInfo {
  final double xeCount;
  final double carbohydrates;
  final double fat;
  final double protein;
  final double calories;

  TotalInfo(
      this.xeCount,
      this.carbohydrates,
      this.fat,
      this.protein,
      this.calories
      );

}

class DashboardModel {
  final TotalInfo totalInfo;
  final List<DishProduct> products;

  DashboardModel({required this.totalInfo, required this.products});
}