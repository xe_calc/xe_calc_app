import 'package:xecalc/models/product.dart';
import 'package:uuid/uuid.dart';

import 'base_local_provider.dart';

class DishProductsLocalDataProvider extends BaseLocalDataProvider {
  final String productKey = 'dish_products';

  Future<List<DishProduct>> getAllProducts() async {
    List<DishProduct> results = [];

    Map<String, dynamic> rawProducts = await read(productKey) ?? {};
    if (rawProducts.isNotEmpty) {
      rawProducts.forEach((key, value) {
        results.add(DishProduct.fromJson(value, key));
      });
    }

    return results;
  }

  Future addProduct(ProductSource source) async {

    Map<String, dynamic> rawProducts = await read(productKey) ?? {};

    String id = Uuid().v4();

    DishProduct newProduct = DishProduct(id, source);
    rawProducts[newProduct.key] = newProduct.toJson();

    await save(productKey, rawProducts);
  }

  Future deleteProduct(String dishProductKey) async {
    Map<String, dynamic>? rawProducts = await read(productKey);

    if (rawProducts != null) {
      rawProducts.remove(dishProductKey);
    }

    await save(productKey, rawProducts);
  }

  Future deleteAllProducts() async {
    await remove(productKey);
  }

  Future updateProduct(String key, ProductSource source) async {
    Map<String, dynamic> rawProducts = await read(productKey) ?? {};
    if (rawProducts.containsKey(key)) {
      DishProduct product = DishProduct.fromJson(rawProducts[key], key);

      product.source = source;

      rawProducts[key] = product.toJson();

      await save(productKey, rawProducts);
    }
  }

}