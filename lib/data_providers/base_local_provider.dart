import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class BaseLocalDataProvider {

  Future<dynamic> read(String key) async {
    final prefs = await SharedPreferences.getInstance();

    String? result = prefs.getString(key);
    if (result != null) {
      return json.decode(result);
    } else {
      return null;
    }

  }

  Future<dynamic> save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  Future<dynamic> remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }


}