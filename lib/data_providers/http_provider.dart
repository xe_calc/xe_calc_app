import 'dart:convert' as convert;

import 'package:http/http.dart' as http;

import 'package:xecalc/models/product.dart';

class HttpProvider {
  final String domain = 'xecalc.ru';
  final String pathPrefix = 'api';
  final int productsLimit = 50;

  Uri getUrl(String path, {Map<String, dynamic>? queryParams}) {
    return Uri.https(domain, '/' + pathPrefix + path, queryParams);
  }

  Future<List<ProductSearchItem>> searchProducts(String name, {int page: 1})
  async {
    int offset = (page - 1) * productsLimit;
    String path = '/products/search/';

    Map<String, dynamic> queryData = {
      'name': name,
      'offset': offset.toString()
    };

    if (offset != 0) {
      queryData['offset'] = offset.toString();
    }

    http.Response response = await http.get(
        getUrl(path, queryParams: queryData)
    );

    List<ProductSearchItem> results = [];
    if (response.statusCode == 200) {
      Map<String, dynamic> responseData = convert
          .jsonDecode(convert.utf8.decode(response.bodyBytes))
          as Map<String, dynamic>;

      if (responseData.containsKey('results') &&
          responseData['results'] is List) {
        responseData['results'].forEach((product){
          String productName = product['name'];
          if (product['brand_name'] != null) {
            productName += ' (${product["brand_name"]})';
          }
          results.add(ProductSearchItem(product['uuid'], productName));
        });
      }
    }

    return results;

  }

  Future<List<ProductPortion>> getProductPortions(String productId) async {
    String path = '/products/$productId/portions/';

    http.Response response = await http.get(getUrl(path));

    List<ProductPortion> results = [];
    if (response.statusCode == 200) {

      List<dynamic> responseData = convert.jsonDecode(
          convert.utf8.decode(response.bodyBytes)
      );

      responseData.forEach((product){
        results.add(ProductPortion(
          product['id'],
          product['name'],
          product['for_weighing'],
          double.tryParse(product['carbohydrates']) ?? 0.0,
          double.tryParse(product['fat']) ?? 0.0,
          double.tryParse(product['protein']) ?? 0.0,
          double.tryParse(product['calories']) ?? 0.0,
          double.tryParse(product['xe_count']) ?? 0.0,
        ));
      });
    }

    return results;
  }
}