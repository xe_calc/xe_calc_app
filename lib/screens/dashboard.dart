import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/data_providers/dish_products_local_provider.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/screens/select_product.dart';
import 'package:xecalc/widgets/dashboard_body.dart';


class DashboardScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => DashboardScreenState();

}

class DashboardScreenState extends State<DashboardScreen> {
  late Future<DashboardModel> model;


  @override
  void initState() {
    super.initState();

    model = loadDashboardData();
  }

  void reloadScreen() {
    setState(() {
      model = loadDashboardData();
    });
  }

  Future<DashboardModel> loadDashboardData() async {
    //await DishProductsLocalDataProvider().deleteAllProducts();
    List<DishProduct> products = await DishProductsLocalDataProvider()
        .getAllProducts();

    double calories = 0;
    double xeCount = 0;
    double carbohydrates = 0;
    double fat = 0;
    double protein = 0;

    if (products.isNotEmpty) {

      products.forEach((DishProduct dishProduct) {
        calories += dishProduct.calories;
        xeCount += dishProduct.xeCount;
        carbohydrates += dishProduct.carbohydrates;
        fat += dishProduct.fat;
        protein += dishProduct.protein;
      });
    }

    return DashboardModel(
      totalInfo: TotalInfo(
          xeCount,
          carbohydrates,
          fat,
          protein,
          calories
      ),
      products: products,
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: model,
      builder: (BuildContext context, AsyncSnapshot<DashboardModel> snapshot){
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(103, 212, 213, 1),
            title: Text('Калькулятор XE'),
          ),
          body: FutureBuilder(
              future: model,
              builder: (BuildContext context,
                  AsyncSnapshot<DashboardModel> snapshot){
                if (snapshot.hasData) {
                  if (snapshot.data != null) {
                    return DashboardBody(snapshot.data!, reloadScreen);
                  }
                  return Center(child: Text('Произошла ошибка при загрузке'));
                }
                if (snapshot.hasError) {
                  return Center(child: Text('Произошла ошибка при загрузке'));
                }
                return Center(child: CircularProgressIndicator());
              }),
          floatingActionButton: FloatingActionButton(
            onPressed: () async {
              await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SelectProduct()
                  )
              );
              reloadScreen();
            },
            child: Icon(Icons.add),
            backgroundColor: Color.fromRGBO(103, 212, 213, 1),
          ),
        );
      }
    );
    }
}