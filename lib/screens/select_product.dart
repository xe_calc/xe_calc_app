import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/screens/search_products.dart';

import '../widgets/custom_product_form.dart';

class SelectProduct extends StatefulWidget {
  @override
  SelectProductState createState() => SelectProductState();
}

class SelectProductState extends State<SelectProduct>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(103, 212, 213, 1),
        title: Text('Добавление продукта'),
        bottom: TabBar(
          indicatorColor: Color.fromRGBO(77, 160, 190, 1),
          tabs: [
            Tab(text: 'Выбор продукта'),
            Tab(text: 'Добавить свой продукт'),
          ],
          controller: tabController,
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          SearchProduct(),
          CustomProductForm(),
        ],
      ),
    );
  }

}