import 'package:flutter/material.dart';
import 'package:xecalc/data_providers/dish_products_local_provider.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/screens/portion_select.dart';
import 'package:xecalc/widgets/card.dart';
import 'package:xecalc/widgets/dashboard/total_card.dart';

import 'custom_product_edit.dart';


class PortionDetail extends StatelessWidget {
  final DishProduct product;

  const PortionDetail(this.product, {Key? key,}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(103, 212, 213, 1),
        title: Text('Продукт'),
      ),
      body: SingleChildScrollView(
        child: Column(
            children: [
              ContentCard(
                child: SizedBox(
                    width: double.infinity,
                    child: Column(
                      children: [
                        Text(
                          product.productName,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        Text(
                          product.portionText,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                          ),
                        )
                      ],
                    )
                )
              ),
              TotalCard(
                calories: product.calories,
                carbohydrates: product.carbohydrates,
                fat: product.fat,
                protein: product.protein,
                xe: product.xeCount,
              ),
              ContentCard(
                child: Column(
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        child: Text('Редактировать'),
                        style: ButtonStyle(
                            backgroundColor:  MaterialStateProperty.all(
                                Color.fromRGBO(103, 212, 213, 1)
                            )
                        ),
                        onPressed: () {
                          if (product.isCustomProduct) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => CustomProductEdit(product)
                              )
                            );
                          } else {
                            CatalogProductSource source = (product.source as
                              CatalogProductSource);

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        PortionForm(
                                          selectedProduct: ProductSearchItem(
                                            source.productId,
                                            product.productName,
                                          ),
                                          selectedPortionId: source.portionId,
                                          portionCount: source.count,
                                          dishProductId: product.key,
                                        )
                                )
                            );
                          }
                        },
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        child: Text('Удалить'),
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Colors.red)
                        ),
                        onPressed: () async {
                          await DishProductsLocalDataProvider()
                            .deleteProduct(product.key);
                          Navigator.of(context).popUntil(
                            (route) => route.isFirst
                          );
                        },
                      ),
                    )
                  ],
                ),
              )
            ]
        ),
      )
    );
  }
}