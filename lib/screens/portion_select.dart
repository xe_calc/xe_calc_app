import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/data_providers/http_provider.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/widgets/portion_form.dart';


class PortionForm extends StatefulWidget {
  final ProductSearchItem selectedProduct;
  final String? dishProductId;
  final int? selectedPortionId;
  final int? portionCount;

  const PortionForm({
    Key? key,
    required this.selectedProduct,
    this.selectedPortionId,
    this.portionCount,
    this.dishProductId,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => PortionFormState();
}


class PortionFormState extends State<PortionForm> {
  late Future<ProductForSelect> product;

  @override
  void initState() {
    super.initState();

    product = loadPortions();
  }

  String get title {
    if (widget.selectedPortionId != null) {
      return 'Редактирование порции';
    } else {
      return 'Добавление порции';
    }
  }

  Future<ProductForSelect> loadPortions() async {
    List<ProductPortion> portions = await HttpProvider()
        .getProductPortions(widget.selectedProduct.key);
    return ProductForSelect(
        widget.selectedProduct.key,
        widget.selectedProduct.name,
        portions
    );

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(103, 212, 213, 1),
          title: Text(title),
        ),
      body: FutureBuilder(
          future: product,
          builder: (BuildContext context,
              AsyncSnapshot<ProductForSelect> snapshot){
            if (snapshot.hasData) {
              if (snapshot.data != null) {
                return PortionEditForm(
                  snapshot.data!,
                  portionCount: widget.portionCount,
                  selectedPortionId: widget.selectedPortionId,
                  dishProductId: widget.dishProductId,
                );
              }
              return Center(child: Text('Произошла ошибка при загрузке'));
            }
            if (snapshot.hasError) {
              return Center(child: Text('Произошла ошибка при загрузке'));
            }
            return Center(child: CircularProgressIndicator());
          }),
    );
  }

}