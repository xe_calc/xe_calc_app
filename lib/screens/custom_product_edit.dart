import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/widgets/custom_product_form.dart';


class CustomProductEdit extends StatelessWidget {
  final DishProduct product;

  const CustomProductEdit(this.product, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Color.fromRGBO(103, 212, 213, 1),
            title: Text('Изменение продукта')
        ),
        body: CustomProductForm(product: product)
    );
  }
}