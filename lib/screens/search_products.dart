import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/data_providers/http_provider.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/widgets/products_search/product_list_item.dart';
import 'package:xecalc/widgets/products_search/product_list_item_load.dart';

class SearchProduct extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SearchProductState();
}


class SearchProductState extends State<SearchProduct> {
  late bool hasNextPage;
  late bool loading;
  late List<ProductSearchItem> products;
  late String query;
  late int currentPage;

  Timer ?_debounce;

  @override
  void initState() {
    hasNextPage = false;
    currentPage = 1;
    products = [];
    loading = false;
    query = '';
    super.initState();
  }

  Future searchProducts() async {
    if (query.length > 2) {
      setState(() {
        loading = true;
      });

      List<ProductSearchItem> result = await HttpProvider()
          .searchProducts(query);

      setState(() {
        products = result;
        loading = false;
        hasNextPage = result.length >= 50;
      });

    }
  }

  Future extendProducts() async {
    int newPage = currentPage + 1;

    List<ProductSearchItem> result = await HttpProvider().searchProducts(
        query,
        page: newPage
    );

    setState(() {
      currentPage = newPage;
      hasNextPage = result.length >= 50;
      products.addAll(result);
    });
  }

  _onSearchChanged(String searchQuery) {
    if (searchQuery == query) {
      return;
    }

    setState(() {
      query = searchQuery;
    });

    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 1000), () {
      searchProducts();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(10),
          child: TextField(
            onChanged: _onSearchChanged,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(77, 160, 190, 1),
                      width: 2
                  )
              ),
              isDense: true,
              hintText: 'Введите название продукта',
            ),
          ),
        ),
        Expanded(
          child: loading ?
          Center(child: CircularProgressIndicator()) :
          ListView.builder(
              shrinkWrap: true,
              itemCount: hasNextPage ? products.length +1 : products.length,
              itemBuilder: (BuildContext context, int index) {
                if(index >= products.length) {
                  extendProducts();
                  return SearchProductListItemLoad();
                }
                return SearchProductListItem(products[index]);
              }
          ),
        )

      ],
    );
  }

  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }

}