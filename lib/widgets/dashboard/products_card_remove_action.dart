import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/data_providers/dish_products_local_provider.dart';


class RemoveAllProductsAction extends StatelessWidget {
  final Function reloadProducts;

  const RemoveAllProductsAction(this.reloadProducts, {Key? key,})
      : super(key: key);

  Future deleteAllProducts() async {
    await DishProductsLocalDataProvider().deleteAllProducts();
    reloadProducts();
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      splashRadius: 15,
      icon: Icon(
        Icons.delete,
        color: Colors.red,
      ),
      onPressed: () async {
        switch (await showDialog<bool>(
            context: context,
            builder: (BuildContext context) {
              return SimpleDialog(
                title: Text('Удалить продукты'),
                children: <Widget>[
                  SimpleDialogOption(
                    onPressed: () { Navigator.pop(context, true); },
                    child: Text('Удалить', style: TextStyle(color: Colors.red),),
                  ),
                  SimpleDialogOption(
                    onPressed: () { Navigator.pop(context, false); },
                    child: Text('Отмена'),
                  ),
                ],
              );
            }
        )) {
          case true:
            deleteAllProducts();
            break;
          case false:
            break;
          case null:
            break;
        }
      },
    );
  }
}