import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xecalc/data_providers/dish_products_local_provider.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/screens/custom_product_edit.dart';
import 'package:xecalc/screens/portion_detail.dart';
import 'package:xecalc/screens/portion_select.dart';

enum ProductItemAction {
  edit,
  delete,
}

class ProductsCardListItem extends StatelessWidget {
  final DishProduct product;
  final Function reloadParent;

  const ProductsCardListItem(this.product, this.reloadParent, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(left: 16, top: 8, bottom: 8),
        child: Row(
          children: [
            Expanded(
              child: GestureDetector(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          product.productName,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 17
                          ),
                        ),
                        SizedBox(height: 2),
                        Text(
                          product.xeCount.toString() + ' XE',
                          style: TextStyle(fontSize: 15),
                        ),
                        SizedBox(height: 2),
                        Text(
                          product.portionText,
                          style: TextStyle(fontSize: 15),
                        )
                      ]
                  ),
                  onTap: () async {
                    await Navigator.push(
                        context, MaterialPageRoute(
                        builder: (context) => PortionDetail(product)
                    ));
                    reloadParent();
                  },
                  behavior: HitTestBehavior.translucent,
                ),
            ),
            Theme(
              data: Theme.of(context).copyWith(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
              ),
              child: PopupMenuButton(

                onSelected: (action) async {
                  if (action == ProductItemAction.delete) {
                    await DishProductsLocalDataProvider()
                        .deleteProduct(product.key);

                  } else if (action == ProductItemAction.edit) {
                    if (product.isCustomProduct) {
                      await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CustomProductEdit(product)
                          )
                      );
                    } else {
                      CatalogProductSource source = (product.source as
                      CatalogProductSource);

                      await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  PortionForm(
                                    selectedProduct: ProductSearchItem(
                                      source.productId,
                                      product.productName,
                                    ),
                                    selectedPortionId: source.portionId,
                                    portionCount: source.count,
                                    dishProductId: product.key,
                                  )
                          )
                      );
                    }

                  }

                  reloadParent();
                },
                itemBuilder: (BuildContext context) {
                  return <PopupMenuEntry<ProductItemAction>>[
                    PopupMenuItem<ProductItemAction>(
                      value: ProductItemAction.edit,
                      child: Text('Редактировать'),
                    ),
                    PopupMenuItem<ProductItemAction>(
                      value: ProductItemAction.delete,
                      child: Text('Удалить'),

                    ),
                  ];
                },
              ),
            )
          ],
        ),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(width: 1.0, color: Color(0xFFe0e5e5))
            )
        ),
    );
  }
}