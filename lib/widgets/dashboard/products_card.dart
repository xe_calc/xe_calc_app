import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/widgets/card.dart';
import 'package:xecalc/widgets/dashboard/product_list_item.dart';
import 'package:xecalc/widgets/dashboard/products_card_remove_action.dart';


class ProductsCard extends StatelessWidget {
  final List<DishProduct> products;
  final Function reloadParent;

  const ProductsCard({
    Key? key,
    required this.products,
    required this.reloadParent
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContentCard(
      title: 'Продукты',
      action: products.length > 0 ?
        RemoveAllProductsAction(this.reloadParent) :
        null,
      titleColor: Color(0xFF5dc2c2),
      bodyPadding: EdgeInsets.all(0),
      child: Column(
        children: List.generate(
            products.length, (index) =>
              ProductsCardListItem(products[index], reloadParent,)
        ),
      )
    );
  }
}