
import 'package:flutter/cupertino.dart';

class TotalItem extends StatelessWidget {
  final String value;
  final String label;

  const TotalItem({
    Key? key,
    required this.value,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          Text(
            value,
            textAlign: TextAlign.center,
            softWrap: true,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
          ),
          SizedBox(height: 5),
          Container(
            child: Text(
              label,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              softWrap: true,
              style: TextStyle(color: Color(0xFFF5A623)),
            ),
          )

        ],
      ),
    );
  }
}