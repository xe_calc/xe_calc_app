import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/widgets/card.dart';
import 'package:xecalc/widgets/dashboard/total_card_item.dart';


class TotalCard extends StatelessWidget {
  final double carbohydrates;
  final double fat;
  final double protein;
  final double calories;
  final double xe;

  const TotalCard({
    Key? key,
    required this.carbohydrates,
    required this.fat,
    required this.protein,
    required this.calories,
    required this.xe
  }) : super(key: key);

  factory TotalCard.fromModel(TotalInfo data) {
    return TotalCard(
      carbohydrates: data.carbohydrates,
      fat: data.fat,
      protein: data.protein,
      calories: data.calories,
      xe: data.xeCount,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ContentCard(
      child: Row(
        children: [
          TotalItem(
            value: calories.toStringAsFixed(1),
            label: 'Калории'
          ),
          TotalItem(
            value: protein.toStringAsFixed(1),
            label: 'Белки'
          ),
          TotalItem(
            value: fat.toStringAsFixed(1),
            label: 'Жиры'
          ),
          TotalItem(
            value: carbohydrates.toStringAsFixed(1),
            label: 'Углеводы'
          ),
          TotalItem(
            value: xe.toStringAsFixed(1),
            label: 'XE'
          ),
        ],
      )
    );
  }

}