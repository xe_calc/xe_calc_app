import 'package:flutter/widgets.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/widgets/dashboard/products_card.dart';
import 'package:xecalc/widgets/dashboard/total_card.dart';

class DashboardBody extends StatelessWidget {
  final DashboardModel model;
  final Function reloadParent;

  const DashboardBody(this.model, this.reloadParent, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
          children: [
            TotalCard.fromModel(model.totalInfo),
            ProductsCard(products: model.products, reloadParent: reloadParent,)
          ]
      ),
    );
  }

}