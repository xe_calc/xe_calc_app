import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/data_providers/dish_products_local_provider.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/utils.dart';

class CustomProductForm extends StatefulWidget {
  final DishProduct? product;

  const CustomProductForm({Key? key, this.product}) : super(key: key);

  @override
  State<StatefulWidget> createState() => CustomProductFormState();

}

class CustomProductFormState extends State<CustomProductForm> {
  DishProduct? product;

  late String name;
  late String measurementLabel;

  late CustomProductMeasurementUnit measurementUnit;
  late double carbohydratesCount;
  late double proteinCount;
  late double fatCount;
  late double caloriesCount;

  late int count;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    name = '';
    carbohydratesCount = 0;
    proteinCount = 0;
    fatCount = 0;
    caloriesCount = 0;
    count = 0;

    if (widget.product != null) {
      measurementUnit = (widget.product!.source as CustomProductSource)
          .measurementUnit;
    } else {
      measurementUnit = CustomProductMeasurementUnit.grams;
    }

    measurementLabel = getMeasurementLabel(measurementUnit);

    if (widget.product != null) {
      product = widget.product;
    }

    super.initState();
  }
  
  String? validateDouble(String? value) {
    if (value != null) {
      return null;
    }
    if (stringToDouble(value!) == null) {
      return 'Введите число';
    }
    return null;
  } 

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(top: 10, left: 10, right: 10),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              initialValue: product?.productName,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Введите название';
                }
                return null;
              },
              onSaved: (value) => {setState(() => {name=value.toString()})},
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Название продукта *',
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromRGBO(77, 160, 190, 1),
                        width: 2
                    )
                ),
                isDense: true,
              ),
            ),
            SizedBox(height: 16,),
            DropdownButtonFormField<CustomProductMeasurementUnit>(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  isDense: true,
                  labelText: 'Порция',
                ),
                value: measurementUnit,
                onChanged: (CustomProductMeasurementUnit? newValue) {
                  if (newValue != null) {
                    setState(() {
                      measurementUnit = newValue;
                      measurementLabel = getMeasurementLabel(newValue);
                    });
                  }
                },
                items: [
                  DropdownMenuItem<CustomProductMeasurementUnit>(
                      value: CustomProductMeasurementUnit.grams,
                      child: Text('Граммы')
                  ),
                  DropdownMenuItem<CustomProductMeasurementUnit>(
                      value: CustomProductMeasurementUnit.milliliters,
                      child: Text('Миллилитры')
                  ),
                ]
            ),
            SizedBox(height: 16,),
            TextFormField(
              initialValue: product?.source.carbohydrates.toString(),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Введите количесвто углеводов';
                }
                if (stringToDouble(value.toString()) == null) {
                  return 'Введите число';
                }
                return null;
              },
              onSaved: (value) => {
                setState(() => {
                  carbohydratesCount = stringToDouble(value.toString()) ?? 0
                })},
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Углеводов на 100 $measurementLabel продукта *',
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromRGBO(77, 160, 190, 1),
                        width: 2
                    )
                ),
                isDense: true,
              ),
            ),
            SizedBox(height: 16,),
            TextFormField(
              initialValue: product?.source.protein.toString(),
              validator: (value) => validateDouble(value),
              onSaved: (value) => {
                setState(() => {
                  proteinCount = stringToDouble(value.toString()) ?? 0
                })},
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Белки на 100 $measurementLabel продукта',
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromRGBO(77, 160, 190, 1),
                        width: 2
                    )
                ),
                isDense: true,
              ),
            ),
            SizedBox(height: 16,),
            TextFormField(
              initialValue: product?.source.fat.toString(),
              validator: (value) => validateDouble(value),
              onSaved: (value) => {
                setState(() => {
                  fatCount = stringToDouble(value.toString()) ?? 0
                })},
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: OutlineInputBorder(),

                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromRGBO(77, 160, 190, 1),
                        width: 2
                    )
                ),
                isDense: true,
                labelText: 'Жиры на 100 $measurementLabel продукта',
              ),
            ),
            SizedBox(height: 16,),
            TextFormField(
              initialValue: product?.source.calories.toString(),
              validator: (value) => validateDouble(value),
              onSaved: (value) => {
                setState(() => {
                  caloriesCount = stringToDouble(value.toString()) ?? 0
                })},
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: OutlineInputBorder(),

                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromRGBO(77, 160, 190, 1),
                        width: 2
                    )
                ),
                isDense: true,
                labelText: 'Калорий на 100 $measurementLabel продукта',
              ),
            ),
            SizedBox(height: 16,),
            TextFormField(
              initialValue: product?.source.count.toString(),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Введите вес';
                }
                if (int.tryParse(value.toString()) == null) {
                  return 'Введите число';
                }
                return null;
              },
              onSaved: (value) => {
                setState(() => {
                  count = int.tryParse(value.toString()) ?? 0
                })},
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromRGBO(77, 160, 190, 1),
                        width: 2
                    )
                ),
                isDense: true,
                labelText: 'Употреблено $measurementLabel *',
              ),
            ),
            SizedBox(height: 16,),
            Text('Поля с * обязательны для заполнения.'),
            SizedBox(height: 16,),
            Container(
              width: double.infinity,
              height: 45,
              child: ElevatedButton(
                onPressed: () async {
                  FormState? formState = _formKey.currentState;
                  if (formState != null && formState.validate()) {
                    formState.save();

                    CustomProductSource productSource = CustomProductSource(
                      productName: name,
                      count: count,
                      measurementUnit: measurementUnit,

                      carbohydrates: carbohydratesCount,
                      calories: caloriesCount,
                      protein: proteinCount,
                      fat: fatCount,
                    );

                    if (product != null) {
                      await DishProductsLocalDataProvider().updateProduct(
                          product!.key,
                          productSource
                      );
                    } else {
                      await DishProductsLocalDataProvider()
                          .addProduct(productSource);
                    }

                    Navigator.of(context).popUntil((route) => route.isFirst);
                  }
                },
                child: Text(product != null ? 'Обновить' : 'Добавить'),
                style: ButtonStyle(
                  backgroundColor:  MaterialStateProperty.all(
                      Color.fromRGBO(103, 212, 213, 1)
                  )
                ),
              ),
            )

          ],
        )
      )
    );
  }

}