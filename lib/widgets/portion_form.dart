import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/data_providers/dish_products_local_provider.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/widgets/card.dart';
import 'package:xecalc/widgets/dashboard/total_card.dart';

import '../utils.dart';


class PortionEditForm extends StatefulWidget {
  final ProductForSelect product;
  final int? selectedPortionId;
  final int? portionCount;
  final String? dishProductId;

  const PortionEditForm(
      this.product,
      {
        Key? key,
        this.selectedPortionId,
        this.portionCount,
        this.dishProductId,
      }) : super(key: key);

  @override
  PortionEditFormState createState() => PortionEditFormState();

}


class PortionEditFormState extends State<PortionEditForm> {
  ProductPortion? selectedPortion;

  double totalCarbohydrates = 0;
  double totalFat = 0;
  double totalProtein = 0;
  double totalCalories = 0;
  double totalXeCount = 0;

  String? countErrorText;

  late TextEditingController countController;

  @override
  void initState() {
    super.initState();
    countController = TextEditingController();

    widget.product.portions.forEach((ProductPortion portion) {
      if (portion.id == widget.selectedPortionId) {
        selectedPortion = portion;
      }
    });

    if (widget.portionCount != null) {
      countController.text = widget.portionCount.toString();
    }

    int? count = int.tryParse(countController.text);
    if (selectedPortion != null && count != null) {
      refreshTotalData(selectedPortion!, count);
    }

    countController.addListener(() {
      if (selectedPortion != null) {
        int? count = int.tryParse(countController.text);

        if (count != null && count > 0) {
          refreshTotalData(selectedPortion!, count);
        } else {
          setState(() {
            totalCalories = 0.0;
            totalCarbohydrates = 0.0;
            totalProtein = 0.0;
            totalFat = 0.0;
            totalXeCount = 0.0;
          });
        }
      }
    });
  }

  @override
  void dispose() {
    countController.dispose();
    super.dispose();
  }

  void refreshTotalData(ProductPortion portion, int count) {
    setState(() {

      totalCalories = calcParam(
          portion.calories,
          count,
          isMeasured: portion.isMeasured
      );

      totalCarbohydrates = calcParam(
          portion.carbohydrates,
          count,
          isMeasured: portion.isMeasured
      );

      totalProtein = calcParam(
          portion.protein,
          count,
          isMeasured: portion.isMeasured
      );

      totalFat = calcParam(
          portion.fat,
          count,
          isMeasured: portion.isMeasured
      );

      totalXeCount = carbohydratesToXe(totalCarbohydrates);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ContentCard(
            child: SizedBox(
              width: double.infinity,
              child: Text(
                widget.product.name,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold
                ),
              )
            ),

          ),
          TotalCard(
            calories: totalCalories,
            carbohydrates: totalCarbohydrates,
            fat: totalFat,
            protein: totalProtein,
            xe: totalXeCount,
          ),
          ContentCard(
            child: Column(
              children: [
                DropdownButtonFormField<ProductPortion>(
                  isExpanded: true,
                  decoration: InputDecoration(
                    isDense: true,
                    labelText: 'Порция',
                  ),
                  onChanged: (ProductPortion? portion) {
                    if (portion != null) {

                      int count;
                      if (portion.isMeasured) {
                        countController.text = '100';
                        count = 100;
                      } else {
                        countController.text = '1';
                        count = 1;
                      }
                      setState(() {
                        selectedPortion = portion;
                      });
                      refreshTotalData(portion, count);

                    } else {
                      setState(() {
                        selectedPortion = portion;

                        totalCalories = 0.0;
                        totalCarbohydrates = 0.0;
                        totalProtein = 0.0;
                        totalFat = 0.0;
                        totalXeCount = 0.0;
                      });
                    }

                  },
                  value: selectedPortion,
                  items: this.widget.product.portions.map
                    <DropdownMenuItem<ProductPortion>>((ProductPortion value) {
                      return DropdownMenuItem<ProductPortion>(
                        value: value,
                        child: Text(
                            value.label,
                            overflow: TextOverflow.ellipsis
                        ),
                      );
                    }).toList(),
                ),
                SizedBox(height: 12),
                TextFormField(
                  controller: countController,
                  decoration: InputDecoration(
                    isDense: true,
                    labelText: 'Количество',
                    errorText: countController.text.length > 0 &&
                        int.tryParse(countController.text) == null ?
                    'Введите целое число' : null,
                  ),
                ),
                SizedBox(height: 16),
                Container(
                  width: double.infinity,
                  height: 45,
                  child: ElevatedButton(
                    onPressed: () async {
                      int count = int.tryParse(countController.text) ?? 0;

                      if (selectedPortion != null && count > 0) {
                        CatalogProductSource source = CatalogProductSource(
                          productName: widget.product.name,
                          productId: widget.product.productId,
                          portionId: selectedPortion!.id,
                          portionName: selectedPortion!.label,

                          fat: selectedPortion!.fat,
                          calories: selectedPortion!.calories,
                          carbohydrates: selectedPortion!.carbohydrates,
                          protein: selectedPortion!.protein,
                          isMeasured: selectedPortion!.isMeasured,
                          count: count,

                        );

                        if (widget.dishProductId != null) {
                          await DishProductsLocalDataProvider()
                              .updateProduct(widget.dishProductId!, source);
                        } else {
                          await DishProductsLocalDataProvider()
                              .addProduct(source);
                        }

                        Navigator.of(context).popUntil(
                                (route) => route.isFirst
                        );
                      }

                    },
                    child: Text(widget.selectedPortionId != null ?
                      'Сохранить' :
                      'Добавить'
                    ),
                    style: ButtonStyle(
                        backgroundColor:  MaterialStateProperty.all(
                            Color.fromRGBO(103, 212, 213, 1)
                        )
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}