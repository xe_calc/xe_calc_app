import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/screens/portion_select.dart';
import 'package:xecalc/widgets/card.dart';

class SearchProductListItem extends StatelessWidget {
  final ProductSearchItem product;

  const SearchProductListItem(this.product, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContentCard(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
        bodyPadding: EdgeInsets.all(0),
        child: InkWell(
            onTap:() {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          PortionForm(
                              selectedProduct: product,
                          )
                  )
              );
            },
            child: Container(
              padding: EdgeInsets.all(16),
              child: Text(product.name),
            )
        )
    );
  }

}