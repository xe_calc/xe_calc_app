import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xecalc/widgets/card.dart';

class SearchProductListItemLoad extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ContentCard(
        bodyPadding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        child: Center(child: LinearProgressIndicator()),
    );
  }

}