
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const EdgeInsets cardPadding = EdgeInsets.all(16);
const EdgeInsets defaultCardMargin = EdgeInsets.fromLTRB(10, 10, 10, 0);

class ContentCard extends StatelessWidget {
  final String? title;
  final Widget? action;
  final Color? titleColor;
  final Widget child;
  final EdgeInsets bodyPadding;
  final EdgeInsets margin;

  const ContentCard(
      {
        Key? key,
        required this.child,
        this.title,
        this.action,
        this.titleColor,
        this.bodyPadding: cardPadding,
        this.margin: defaultCardMargin,
      }) : super(key: key);

  Widget getTemplate() {
    if (title != null || action != null) {
      return Column(
        children: [
          ContentCardTitle(
            text: title,
            action: action,
            color: titleColor ?? Colors.black,
          ),
          ContentCardBody(child, bodyPadding)
        ],
      );
    } else {
      return ContentCardBody(child, bodyPadding);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: margin,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(2.0)),
        side: BorderSide(width: 1.0, color: Color(0xFFe0e5e5))
      ),
      child: getTemplate()
    );
  }
}


class ContentCardTitle extends StatelessWidget {
  final String? text;
  final Widget? action;
  final Color color;

  const ContentCardTitle({
    Key? key,
    this.text,
    required this.color,
    this.action
  }) : super(key: key);

  Widget getTemplate() {
    Widget? titleText;

    if (text != null) {
      titleText = Text(
        text!,
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
          color: color,
        ),
      );
    }

    if (titleText != null && action != null) {
      return Row(
        children: [
          Expanded(
            child: titleText
          ),
          action!
        ],
      );
    } else if (titleText != null) {
      return titleText;
    } else {
      return Expanded(
          child: action!
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(width: 1.0, color: Color(0xFFe0e5e5)))
      ),
      padding: action != null ?
        cardPadding.copyWith(right: 0, top: 0, bottom: 0) :
        cardPadding,
      alignment: Alignment.centerLeft,
      child: getTemplate(),
    );
  }
}

class ContentCardBody extends StatelessWidget {
  final Widget content;
  final EdgeInsets padding;

  const ContentCardBody(this.content, this.padding, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      child: content,
    );
  }
}