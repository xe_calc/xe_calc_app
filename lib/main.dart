import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:xecalc/screens/dashboard.dart';
import 'package:sentry_flutter/sentry_flutter.dart';



void main() async {
  await SentryFlutter.init(
        (options) => options.dsn = 'https://9f3f32da37544070bd6ba3456511f1ad'
            '@o194924.ingest.sentry.io/5847421',
    appRunner: () => runApp(XeCalcApp()),
  );
}

class XeCalcApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      title: 'Калькулятор хлебных единиц',
      home: DashboardScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
