import 'package:flutter/widgets.dart';
import 'package:xecalc/models/product.dart';
import 'package:xecalc/data_providers/dish_products_local_provider.dart';

class DashboardViewModel extends ChangeNotifier {
  bool loading = false;
  List<DishProduct> products = [];
  double totalXeCount = 0;
  double totalCarbohydrates = 0;
  double totalFat = 0;
  double totalProtein = 0;
  double totalCalories = 0;

  DishProductsLocalDataProvider productsLocalProvider =
    DishProductsLocalDataProvider();

  Map<String, double> get totalDataAsMap{
    return {
      'calories': totalCalories,
      'carbohydrates': totalCarbohydrates,
      'fat': totalFat,
      'protein': totalProtein,
      'xe': totalXeCount
    };
  }

  Future getProductsAndCalcTotal() async {
    products = await productsLocalProvider.getAllProducts();

    if (products.isNotEmpty) {
      totalCalories = 0;
      totalXeCount = 0;
      totalCarbohydrates = 0;
      totalFat = 0;
      totalProtein = 0;

      products.forEach((DishProduct dishProduct) {
        totalCalories += dishProduct.calories;
        totalXeCount += dishProduct.xeCount;
        totalCarbohydrates += dishProduct.carbohydrates;
        totalFat += dishProduct.fat;
        totalProtein += dishProduct.protein;
      });
    }
  }
}