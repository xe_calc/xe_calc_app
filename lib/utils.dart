import 'models/product.dart';

double roundDouble(double rawValue){
  return double.parse((rawValue).toStringAsFixed(1));
}

double carbohydratesToXe(double carbohydratesCount){
  return roundDouble(carbohydratesCount / 12);
}

double calcParamForMeasured(double paramCount, int count){
  return roundDouble((paramCount * count) / 100);
}

double calcParam(double paramCount, int count, {bool isMeasured: false}){
  if (isMeasured) {
    return calcParamForMeasured(paramCount, count);
  } else {
    return paramCount * count;
  }
}

String getMeasurementLabel(CustomProductMeasurementUnit measurementUnit) {
  switch (measurementUnit) {
    case CustomProductMeasurementUnit.grams:
      return 'гр.';
    case CustomProductMeasurementUnit.milliliters:
      return 'мл.';

  }
}

double? stringToDouble(String value) {
  return double.tryParse(value.replaceAll(',', '.'));
}